package main

import (
	"bufio"
	"context"
	"io"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	crypto "bitbucket.org/cryptoguyin/herdius/crypto"
	pb "bitbucket.org/cryptoguyin/herdius/proto"

	"google.golang.org/grpc"
)

func main() {
	rand.Seed(time.Now().Unix())

	// creating a random key pair
	err := crypto.Random()

	if err != nil {
		log.Fatalf("err creating random key pair %v", err)
	}

	// dail server
	conn, err := grpc.Dial(":3000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("can not connect with server %v", err)
	}

	// create stream
	client := pb.NewMathClient(conn)
	stream, err := client.Max(context.Background())
	if err != nil {
		log.Fatalf("openn stream error %v", err)
	}

	var max int32
	ctx := stream.Context()
	done := make(chan bool)

	reader := bufio.NewReader(os.Stdin)

	// first goroutine sends numbers from stdin to stream
	// enter q to quit
	go func() {
		log.Println("enter numbers followed by return. q to quit")
		for {
			// generate random nummber and send it to stream
			text, _ := reader.ReadString('\n')
			text = strings.Replace(text, "\n", "", -1)
			if strings.Compare(text, "q") == 0 || strings.Compare(text, "") == 0 {
				break
			}
			// create a signed signature
			sig := crypto.Sign(text)
			num, err := strconv.ParseInt(text, 10, 32)
			if err != nil {
				log.Fatalf("ParseInt fail %v", err)
			}
			pub := crypto.PublicKey()
			req := pb.Request{Num: int32(num), Signature: sig, PublicKey: pub}
			if err := stream.Send(&req); err != nil {
				log.Fatalf("stream send err %v", err)
			}
			log.Printf("%d sent", req.Num)
			time.Sleep(time.Millisecond * 200)
		}
		if err := stream.CloseSend(); err != nil {
			log.Println(err)
		}
	}()

	// receives data from stream and saves result in max variable
	go func() {
		for {
			resp, err := stream.Recv()
			// if stream is finished it closes done channel
			if err == io.EOF {
				close(done)
				return
			}
			if err != nil {
				log.Fatalf("can not receive %v", err)
			}
			max = resp.Result
			log.Printf("new max %d received", max)
		}
	}()

	// if context is done closes done channel
	go func() {
		<-ctx.Done()
		if err := ctx.Err(); err != nil {
			log.Println(err)
		}
		close(done)
	}()

	<-done
	log.Printf("Last max int = %d", max)
}
