package crypto

import (
	"crypto/rand"
	"encoding/base64"
	"strings"

	"golang.org/x/crypto/ed25519"
)

// Crypto stores key pair
type Crypto struct {
	PrivateKey []byte
	PublicKey  []byte
}

var crypt Crypto

// Random to create a random crypto pair
func Random() error {
	token, err := generateRandomString(32)
	if err != nil {
		return err
	}
	rand := strings.NewReader(token)
	pub, pri, err := ed25519.GenerateKey(rand)
	if err != nil {
		return err
	}
	crypt.PrivateKey = pri
	crypt.PublicKey = pub
	return nil
}

// PublicKey returns PublicKey
func PublicKey() []byte {
	return crypt.PublicKey
}

// Sign a message
func Sign(msg string) []byte {
	sig := ed25519.Sign(crypt.PrivateKey, []byte(msg))
	return sig
}

// Verify a message
func Verify(msg, publicKey, sig []byte) bool {
	return ed25519.Verify(publicKey, msg, sig)
}

// helpers

func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func generateRandomString(s int) (string, error) {
	b, err := generateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}
