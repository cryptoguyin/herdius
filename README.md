# FindMaxNumber RPC Bi-Directional Streaming Client and Server system

- The function takes a stream of Request message that has one integer, and returns a stream of Responses that represent the current maximum between all these integers
- Client will be having a cryptographic public key and client will be identified using his private key. Client will sign every request message in the stream
- Each requested message should be verified against the signature at the server end. Only those numbers will be considered to be processed whose sign is successfully verified

```
Example: The client will send a stream of number (1,5,3,6,2,20) and each number will be signed by the private key of the client and the server will respond with a stream of numbers (1,5,6,20)
```

## Requirements

- go 1.9
- glide
- protobuf
- go support for protobuf

## Signature

- Implements the Ed25519 signature algorithm (https://godoc.org/golang.org/x/crypto/ed25519)
- Client generates a random Ed25519 key pair (PublicKey and PrivateKey)
- Client sends the Number, PublicKey and Signed signature of Number with PrivateKey
- Server verifies the Number with PublicKey and Signature

## Installing

```
brew install go
brew install glide
brew install protobuf
go get -u github.com/golang/protobuf/protoc-gen-go
```

## Running

```
make dep
make all
```

It should create two binaries `server` and `client` in the bin directory

Start server `./bin/server` and in other terminal start `./bin/client`

## Sample Output

```
./bin/client
2019/02/28 22:43:54 enter numbers followed by return. q to quit
1
2019/02/28 22:44:09 1 sent
2019/02/28 22:44:09 new max 1 received
5
2019/02/28 22:44:10 5 sent
2019/02/28 22:44:10 new max 5 received
3
2019/02/28 22:44:11 3 sent
6
2019/02/28 22:44:12 6 sent
2019/02/28 22:44:12 new max 6 received
2
2019/02/28 22:44:12 2 sent
20
2019/02/28 22:44:13 20 sent
2019/02/28 22:44:13 new max 20 received
q
2019/02/28 22:44:21 Last max int = 20
2019/02/28 22:44:21 context canceled
```

```
./bin/server
2019/02/28 22:43:54 start new server
2019/02/28 22:44:09 new max 1 send
2019/02/28 22:44:10 new max 5 send
2019/02/28 22:44:12 new max 6 send
2019/02/28 22:44:13 new max 20 send
2019/02/28 22:44:21 exit
```
