
dep:
	glide install

protoc:
	cd proto && protoc --go_out=plugins=grpc:. *.proto

server: protoc
	go build -i -o ./bin/server ./server

client: protoc
	go build -i -o ./bin/client ./client

all: client server

.PHONY: client server protoc dep
